#!/usr/bin/env python3

# Script for determining if file(s) are opened/locked by another program
# Will output "filename: <pid> <pid> ..." if opened/locked by the pids
# Python reimplementation of the fuser command
# Rasmus J. Have 2022-06-19

import sys
import os
import glob

# If we don't get any arguments then give usage
if len(sys.argv) == 1:
    sys.exit(1)

# Only support one argument for now, -a to show status on all files, also files that aren't opened by any process
if sys.argv[1] == '-a':
    filesraw = sys.argv[2:]
else:
    filesraw = sys.argv[1:]

# Files that we want to keep track on
files=[]
# List of files that haven't been found yet
filesnotfound=[]
# List of pids for each file we're keeping track of
files_pids={}

# Build file lists from arguments
for f in filesraw:
    files.append(os.path.abspath(f))
    filesnotfound.append(os.path.abspath(f))

# Run through /proc to see which processes has what files opened
for l in glob.glob("/proc/*/fd/*"):
    if not os.access(l,os.R_OK):
        continue
    rfn=os.readlink(l)
    if rfn.startswith("socket") or rfn.startswith("pipe") or rfn.startswith("/proc/") or rfn.startswith("/memfd:"):
        continue
    if rfn in files:
        pid = l[6:6+l[6:].find("/")]
        if rfn in files_pids:
            files_pids[rfn].append(pid)
        else:
            files_pids[rfn] = [pid]
        if rfn in filesnotfound:
            filesnotfound.remove(rfn)

# Print files and pids
for f in files_pids:
    print(f+":", end='')
    for pid in files_pids[f]:
        print(" " + pid, end='')
    print()

# If -a was given as first argument, then also show all the files that where not open
if sys.argv[1] == '-a':
    for f in filesnotfound:
        print(f+':')

# Mimic the fuser exit values: 0 if one of the files where open, 1 if none of the files where open
if len(files_pids)==0:
    sys.exit(1)
else:
    sys.exit(0)
