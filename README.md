# fuser.py

A fuser implementation in python for Linux. This tool will show you which processes (pids) has a file open.

The tool is written for python3 and has no external dependencies.

## Usage

Basic usage:
```
$ fuser.py file
file: 1234 1235
```

Show output for all arguments where file1 is opened by two processes, and file2 and file3 are not opened by any processes:
```
$ fuser.py -a file1 file2 file3
file1: 1234 1235
file2:
file3:
```

## License
MIT License
